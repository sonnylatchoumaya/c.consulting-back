<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="author" content="Sonny" />
    <meta
      name="keywords"
      content="sarl,c.consulting, cconsulting"
    />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SARL C. Consulting</title>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="img/favicon.ico" />
    <link
      href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap"
      rel="stylesheet" />
  </head>

  <body>
    <header class="header-container">
      <!-- Logo -->
      <div class="logo">
        <!-- Lien -->
        <a href="index.html">
          <!-- Image -->
          <img src="../img/logo.png" alt="Logo" />
        </a>
      </div>

      <!-- Navigation -->
      <nav>
        <div class="menu-burger">
          <div class="icon" onclick="display()">
            <img src="../img/burger.png" alt="Menu" />
          </div>
          <div class="menu" id="menu">
            <a href="../index.html">Accueil</a>
            <a href="../page/article.html">Article</a>
            <a href="../page/contact.html">Contact</a>
            <a href="../page/a-propos.html">À propos</a>
            <a href="../page/produit.html">Produit</a>
            <a href="../page/mention-legal.html">Mentions légales</a>
          </div>
        </div>
        <script type="text/javascript">
          var menu_burger = document.getElementById("menu");
          var show = 0;

          function display() {
            if (show == 0) {
              menu_burger.style.display = "block";
              show = 1;
            } else {
              menu_burger.style.display = "none";
              show = 0;
            }
          }
        </script>
      </nav>
    </header>
    <main>
      <section class="">
        <div class="banner">
        <h1>Contactez nous !</h1>
        </div>
      </section>
      <div class="testbla">
        <div class="global-contact">
          <div class="form-mess">
            
            <form action="contact.php" method="POST" class="form-contact">
              <div class="form-contact-input">
                <input
                  type="text"
                  name="namemsg"
                  id="namemsg"
                  placeholder="nom..."
                  required
                />
              </div>
              <div class="form-contact-input">
                <input
                  type="email"
                  name="emailmsg"
                  id="emailmsg"
                  placeholder="e-mail..."
                  required
                />
              </div>
             
              <div class="form-contact-input">
                <input class="msg"
                  type="text"
                  name="msg"
                  id="msg"
                  placeholder="message..."
                  required
                />
              </div>
              <div class="form-contact-submit">
                <input onclick="return controle();" type="submit" name="submit" value="Envoyer" />
              </div>
            </form>
              <?php /*
            if(isset($_POST['msg'])){
                $entete  = 'MIME-Version: 1.0' . "\r\n";
                $entete .= 'Content-type: text/html; charset=utf-8' . "\r\n";
                $entete .= 'From: ' . $_POST['emailmsg'] . "\r\n";
        
                $message = '<h1>Message envoyé depuis la page Contact de monsite.fr</h1>
                <p><b>Nom : </b>' . $_POST['namemsg'] . '<br>
                <b>Email : </b>' . $_POST['emailmsg'] . '<br>
                <b>Message : </b>' . $_POST['msg'] . '</p>';
        
                $retour = mail('sonnylatchoumaya@gmail.com', 'Envoi depuis page Contact', $message, $entete);
                if($retour) {
                    echo '<p>Votre message a bien été envoyé.</p>';
                }
            } */
            ?> 
          </div>
          <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1854.8961370259112!2d2.232270205840233!3d48.89215814413795!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e664fd95250cd3%3A0xa49f0615771e1cba!2s1%20Cours%20Valmy%2C%2092800%20Puteaux!5e0!3m2!1sfr!2sfr!4v1625344881897!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
          </div>
        </div>
        
      </div>
    </main>

    <footer>
      <div class="footer-container">
        <div class="block-par-4">
          <!-- logo et pitch -->
          <a href="index.html">
            <img src="../img/logo1.jpg" class="footer-logo" />
          </a>
        </div>
        <div class="block-par-4">
          <!-- navigation -->
          <h4>Navigation</h4>
          <nav>
            <ul>
              <li><a href="index.html">Accueil</a></li>
              <li><a href="page/article.html">Article</a></li>
              <li><a href="page/contact.html">Contact</a></li>
              <li><a href="page/a-propos.html">À propos</a></li>
              <li><a href="page/produit.html">Produit</a></li>
              <li><a href="page/mention-legal.html">Mention Légal</a></li>
              
            </ul>
          </nav>
        </div>
        <div class="block-par-4">
          <!-- contact -->
          <h4>Contactez-nous</h4>
          <p>
            Adresse<br />
            <a href="mailto:">Email</a><br />
            Tel
          </p>
        </div>
        <div class="block-par-4">
          <!-- réseaux sociaux -->
          <h4>Réseaux sociaux</h4>
          <div class="socials-img">
            <a href="https://www.linkedin.com/company/sarl-c.consulting/" target="_blank"
              ><img
                src="https://upload.wikimedia.org/wikipedia/commons/c/ca/LinkedIn_logo_initials.png"
                alt="LinkedIn"
            /></a>
            
          </div>
          <div>
            <p class="copyright">
              © C.Consulting, 2021 - Tous droits réservés
            </p>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>